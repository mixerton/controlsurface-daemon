
ControlSurface-Daemon: A  Mapper Daemon for Novation Control Surfaces and others where management is needed.

A daemon that exports an ALSA midi client that speaks over a private USB connection to a studio control surface.

//
// ControlSurface-Daemon: A Mapper Daemon for Novation Control Surfaces and others where management is needed.

//
//
// (C) 2014 David J Greaves - University of Cambridge / Mixerton ST
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//

