// ControlSurface-Daemon - 14th Jan 2014
//
// ControlSurface-Daemon: A  Mapper Daemon for Novation Control Surfaces and others where management is needed.
//
// A daemon that exports an ALSA midi client that speaks over a private USB connection to a studio control surface
//
// (C) 2014 David J Greaves - University of Cambridge / Mixerton ST
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//


#include <libusb.h>
#include <alsa/asoundlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <syslog.h>

#include "daemon.h"

FILE *logfd = 0;

static snd_seq_t *seq_handle = 0;
static int alsa_in, alsa_out;
static libusb_device_handle *xdev = 0;
static libusb_context *ctx = 0;

bool silentf = 0;
bool noretryf = 0; // If being run by a udev rule on each usb insert do not want to retry since udev will relaunch us. // Could check pid of last one in /var/run or something to automate this?

extern const char *banners[SOFT_CONTEXTS]; // Titles for each context.

#define DEBUG(X)  if (!silentf && logfd) X  
#define DEBUG2(X)


int MMC_codes[MMC_INDEXES]; // Mapping of index to sysex wire MMC encoding

int inverted_cc_index[16][128];
int inverted_mmc_index[MMC_INDEXES];
int softctx = 0;
int xport_keys_enabled = 1;  // Toggled on/off by the tranpost lock button on the surface
int daemon_enabled = 1;      // Toggled on/off by the automap key on the surface.

typedef enum {
  S_COLD,
  S_CONNECT,
  S_REFRESH,
  S_RUN,
  S_EXIT
} runmode_t;

volatile runmode_t runmode = S_COLD;


void lcd_update();

struct knob_s // Each physical entity on the control surface has one of these structures.
{
  const char *name;
  signed char feedback_ctrl;     
  unsigned char physical_position; // Last value received from surface
  // After a soft ctx change, pickup will potentially be needed on all sliders and pots.
  char pickup_running;             // Direction of pickup or zero if pickup achieved or off.
  unsigned short knob_flags;       // Flags relating to the physical knob rather than to its 
  unsigned char companion_ctrl;    // Certain knobs have companion leds etc next to them that are addressed by this.
  bool ignored;

  struct soft_s // Each virtual behaviour of a physical entity has one of these.
  {
    char MMC_index;                  // If non-negative, convert to midi machine control intstead of parameter change.
    unsigned char last_notification; // Last value sent if controllable
    unsigned short flags;
    unsigned char fwd_chan;
    unsigned char fwd_ctrl;
    unsigned char pickup_target;     // Value the physical controller must reach or exceed for pickup.
    bool pickup_set; 
  } softs[SOFT_CONTEXTS];

  void remote_flying_update(int vale); // Flying fader or led status remote update
  void flush_to_surface(bool drainf=true);
  void local_update(int vale, int cc);
  
  knob_s() // uctor
  {
    ignored = 0;
    feedback_ctrl = -1;
  }
} ;


// For future surfaces with more than one channel in use we may need the following times 16.
struct knob_s Knobs[128]; // Indexed by controller number on the physical surface. 

class usbtx_t
{
  unsigned char obuf[MAXL];
  int bufuse;
  bool hi;
  pthread_mutex_t mutex;
public:
  void flush();
  void fmt_cc(int channel, int cc, int vale);
  usbtx_t() // ctor
  {
    pthread_mutex_init(&mutex, 0);
    bufuse = 0;
  }
  void send_cc(int channel, int cc, int vale); // Update something on the control surface.
  int send_sysex(const unsigned char *msg, int len);
};

usbtx_t usbtx;

void usbtx_t::send_cc(int channel, int cc, int vale) // Update something on the control surface.
{ 
  pthread_mutex_lock(&mutex);
  fmt_cc(channel, cc, vale);
  flush();
  pthread_mutex_unlock(&mutex);
}

void usbtx_t::fmt_cc(int channel, int cc, int vale) // Update something on the control surface.
{
  assert(channel >=0 && channel <= 15);
  assert(cc >=0 && cc <= 127);
  assert(vale >=0 && vale <= 127);
  if (bufuse >= MAXL-4) flush();
  obuf[bufuse+0] = 0xb | (wire_number << 4);
  obuf[bufuse+1] = channel | MIDI_PARAMETER;
  obuf[bufuse+2] = cc;
  obuf[bufuse+3] = vale;
  DEBUG(fprintf(logfd, "msg to surface: fmt_cc %02X %02X %02X\n", obuf[bufuse+1], obuf[bufuse+2], obuf[bufuse+3]));
  bufuse += 4;
}


int usbtx_t::send_sysex(const unsigned char *msg, int len)
{
  pthread_mutex_lock(&mutex);
  int rc = -1000;
  assert(len * 4 < MAXL * 3);
  unsigned char buffer[MAXL];
  int p, d = 0;
  assert(msg[0]==0xF0);
  for (p=0; p<len-2; p+=3)
    {
      buffer[d++] = 0x4 | (wire_number << 4);
      buffer[d++] = msg[p];
      buffer[d++] = msg[p+1];
      buffer[d++] = msg[p+2];
    }
  if (len - p == 1)
    {
      buffer[d++] = 0x5 | (wire_number << 4);
      buffer[d++] = msg[p];
    }
  else if (len - p == 2)
    {
      buffer[d++] = 0x6 | (wire_number << 4);
      buffer[d++] = msg[p];
      buffer[d++] = msg[p+1];
    }
  DEBUG2(for (int i= 0; i<d; i++) printf("%02X ", buffer[i]));
  int actual_length = d;
  if (runmode > S_CONNECT && runmode < S_EXIT)
    {
      rc = libusb_bulk_transfer(xdev, endpoint, buffer, d, &actual_length, 0);
      //DEBUG(fprintf(logfd, "usb_send_sysex: Sent with rc=%i,  bytes=%i\n", r, actual_length));
    }
  pthread_mutex_unlock(&mutex);
  return rc;
}

void usbtx_t::flush()
{
  if (runmode > S_CONNECT && runmode < S_EXIT)
    {
      int actual_length = bufuse;
      int r = libusb_bulk_transfer(xdev, endpoint, obuf, bufuse, &actual_length, 0);
      DEBUG2(printf("usb_send_cc: surface obuf sent %02X %02X %02X with rc=%i,  bytes=%i\n", obuf[1], obuf[2], obuf[3], r, actual_length));
    }
  bufuse = 0;
}

void flush_to_surface()
{
  lcd_update();
  for (int i=0;i<128; i++) Knobs[i].flush_to_surface(false);
  usbtx.flush();
}

class flashx_t
{ 
  volatile bool hi;
  volatile int countdown;
  volatile int flasher;
public:
  void run10Hz();

  flashx_t() // ctor
    {
      countdown = 0;
      flasher = 0;
    }
  void stop();
  void trigger(bool hi)
  {
    this->hi = hi;
    countdown = 15; // keep flashing for 1.5 seconds.
  }
};

flashx_t flashx;



int open_novation_usb_dev()
{
  xdev = libusb_open_device_with_vid_pid (ctx, manu, devid);
  if (!xdev) 
    {
      if (getuid()) printf("Not run as root\n");
      printf ("uid=%i:Cannot open control surface USB device %04x:%04x\n", getuid(), manu, devid);
      return -2;
    }
  DEBUG(fprintf(logfd, "Opened target midi device ok\n"));  
  //libusb_reset_device(xdev);
  int rc = libusb_claim_interface (xdev, subifc);
  DEBUG(fprintf(logfd, "claim interface %i rc=%i\n", subifc, rc));
  if (rc) 
    {
      printf ("Cannot claim endpoint %i in USB device %04x:%04x ", subifc,  manu, devid);
      return -3;
    }
  DEBUG(fprintf(logfd, "Opened usb connection to Novation controller\n"));  
  return 0;
}





void *flash(void *arg)
{
  while(runmode != S_EXIT)
    {
      flashx.run10Hz();
      usleep(100*1000);
    }

  return 0;
}

void alsa_send_cc(int channel, int cc, int vale)
{
  DEBUG(fprintf(logfd, "alsa_send_cc   Surface->ALSA  ch=%i cc=0x%x vale=%i\n", channel+1, cc, vale));
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_source(&ev, alsa_out);
  snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_direct(&ev);
  snd_seq_ev_set_controller(&ev, channel, cc, vale);
  int space = snd_seq_event_output_direct(seq_handle, &ev);
  if (space < 0)
    printf("output error %i %s\n", space, snd_strerror(space));
  //else printf("Spaces left %i\n", space);
  //snd_seq_drain_output(seq_handle);
}      

void alsa_send_note(bool onf, int channel, int nn, int vale)
{
  DEBUG(fprintf(logfd, "alsa_send_note ch=%i+1 nn=0x%x vale=%i\n", channel, nn, vale));
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_source(&ev, alsa_out);
  snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_direct(&ev);
  if (onf) snd_seq_ev_set_noteon(&ev, channel, nn, vale);
  else snd_seq_ev_set_noteoff(&ev, channel, nn, vale);
  int space = snd_seq_event_output_direct(seq_handle, &ev);
  if (space < 0)
    printf("output error %i %s\n", space, snd_strerror(space));
  //else printf("Spaces left %i\n", space);
  //snd_seq_drain_output(seq_handle);
}      

void alsa_send_midi_machine_control(int code, int gotow)
{
  DEBUG(fprintf(logfd, "Sending MMC code=%i goto=%i\n", code, gotow));
  unsigned char buf[13];
  int len = 0;
  if (gotow >= 0)
    {
      // Compare with : F0 7F 7F 01 01 hh mm ss ff F7 which is the MTC seek.
      unsigned char raw[13] = {  0xF0, 0x7F, 0x7f, 0x06, 0x44, 0x06, 0x01, 00, 00, 00, 00, 0, 0xF7 }; // Transport seek GOTO.
      for (int i=0; i<7;i++) buf[i] = raw[i];
      for (int i=11; i<13;i++) buf[i] = raw[i];
      buf[10] = (gotow / 40) % 25; // frame
      buf[9] = (gotow / 1000) % 60; // second
      buf[8] = (gotow / (1000*60)) % 60; // minute
      buf[7] = (gotow / (1000*3600)) % 24; // Hour
      len = 13;
    }
  else       // code is MIDI_MMC_Stop,  MIDI_MMC_Play ... etc
    {
      unsigned char raw[6] = {  0xF0, 0x7F, 0x7f, 0x06, 0x0, 0xF7 };
      for (int i=0; i<7;i++) buf[i] = (i==4) ? code: raw[i];
      len = 6;
    }

  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_source(&ev, alsa_out);
  snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_direct(&ev);
  snd_seq_ev_set_sysex(&ev, len, buf);
  snd_seq_event_output_direct(seq_handle, &ev);
}


void change_ctx(int nv)
{
  softctx = nv;
  flush_to_surface();
}


void usb_rx_cc(int cc, int vale) // A change is received from the control surface
{
  assert(cc>=0 && cc<=127);
  DEBUG2(printf("usb rx_cc decimals: %i %i\n", cc, vale));

  if (cc == novation_xport_button_cc)
    {
      xport_keys_enabled = vale;
      DEBUG(fprintf(logfd, "Toggle xport mode  %i  %i\n", cc, vale));
      return;
    }

  if ((Knobs[cc].knob_flags & KF_BIT6) && (vale & 0x40))
    {
      vale = vale & 0x3F;
      cc = Knobs[cc].companion_ctrl;
      DEBUG(fprintf(logfd, "Redirect on bit 6 to controller decimal %i\n", cc));
      DEBUG(fprintf(logfd, "Redirected on bit 6 to controller %s vale =%i\n", Knobs[cc].name, vale));
    }

  struct knob_s &k = Knobs[cc];  
  k.local_update(vale, cc);
}

void knob_s::local_update(int vale, int cc)
{
  if (knob_flags & (KF_CTX_DIAL|KF_CTX_UP|KF_CTX_DOWN))
    {
      int delta = 
	knob_flags & KF_CTX_UP ? 1:
	knob_flags & KF_CTX_DOWN ? -1:
	(vale & 0x40) ? -1: 1;
      int nv = softctx + delta;
      if (nv > SOFT_CONTEXTS-1) nv = SOFT_CONTEXTS-1;
      else if (nv < 0) nv = 0;
      DEBUG(fprintf(logfd, "context change delta=%i nv=%i\n", delta, nv));
      if (nv != softctx)
	{
	  change_ctx(nv);
	}
      return;
    }
  if (!name)
    {
      DEBUG(if (!ignored) printf("Ignoring controller code 0x%X from surface\n", cc));
      ignored = true;
      return;
    }

  if (knob_flags & KF_BOOL)
    {
      int nv = vale ? 127:0;
      physical_position = nv;
      vale = nv;
    }
  else if (knob_flags & KF_NOVATION_ENCODER)  // These infinite rotation pots have a relative encoding
    {
      int delta = (vale & 64) ? -(vale & 0x3F): vale;
      int nv = physical_position + delta;
      if (nv < 0) nv = 0; else if (nv > 127) nv = 127;
      physical_position = nv;
      vale = nv;
    }
  else physical_position = vale;
  DEBUG2(printf("Knob %s vale %i\n", name, vale));
  struct knob_s::soft_s &s = softs[softctx];
  if (s.MMC_index > 0)
    {
      int code = MMC_codes[s.MMC_index];
      // Only send mmc on the active pressdown edge.
      if (vale > 0) alsa_send_midi_machine_control(code, -1);
    }
  else if (s.flags & F_FWD_TO_ALSA)
    {
      if (s.pickup_set)
	{
	  s.pickup_set = false;
	  pickup_running = (s.pickup_target > physical_position) ? 1:(s.pickup_target < physical_position) ? -1: 0;
	  DEBUG(fprintf(logfd, "start pickup on %s pol %i\n", name, pickup_running));
	  flashx.trigger(pickup_running>0);
	}

      if (pickup_running > 0)
	{
	  // If we are in pickup, do not emit, just update local copy until we leave pickup.
	  DEBUG(fprintf(logfd, "in pickup pol=%i for %s  %i -> %i\n", pickup_running, name, physical_position, s.pickup_target));
	  if (physical_position >= s.pickup_target)
	    {
	      DEBUG(fprintf(logfd, "End of pickup for %s\n", name));
	      pickup_running = 0;
	      flashx.stop();
	    }
	  else flashx.trigger(pickup_running>0); // keep flashing while doing a pickup
	}
      else if (pickup_running < 0)
	{
	  // If we are in pickup, do not emit, just update local copy until we leave pickup.
	  DEBUG(fprintf(logfd, "in pickup pol=%i for %s  %i -> %i\n", pickup_running, name, physical_position, s.pickup_target));
	  if (physical_position <= s.pickup_target)
	    {
	      DEBUG(fprintf(logfd, "End of pickup for %s\n", name));
	      pickup_running = 0;
	      flashx.stop();
	    }
	  else flashx.trigger(pickup_running>0); // keep flashing while doing a pickup
	}

      if (!pickup_running) alsa_send_cc(s.fwd_chan, s.fwd_ctrl, vale);
    } 
}



void usb_dispatch()
{
  unsigned char data[4096];
  int actual_length = 0;
  int rc = libusb_bulk_transfer(xdev, 0x80 | endpoint, data, sizeof(data), &actual_length, 0);
  DEBUG2(if (!actual_length != 4) printf("usb_dispatch: rxd rc=%i bytes %i\n", rc, actual_length));
  if (rc == 0)
    {
      for (int i=0; i<actual_length; i+= 4)
	{
	  //DEBUG(fprintf(logfd, "usb rx %02x %02x %02x %02x\n", data[i],  data[i+1], data[i+2], data[i+3]));
	  switch (data[i] & 0xF)
	    {
	    case 0xb: // Parameter change
	      {
		int cc = data[i+2];
		int vale = data[i+3]; 
		if (cc == 0x7b) // all notes off
		  {
		    // ignore
		  }
		if (cc == novation_automap_button_cc) // 0x6b
		  {
		    if (vale && !daemon_enabled) 
		      {	  flush_to_surface();
		      }
		    daemon_enabled = vale;
		    DEBUG(fprintf(logfd, "Automap mode now enabled=%i\n", vale));
		  }
		else if (daemon_enabled) usb_rx_cc(cc, vale);
	      }
	      break;
	    
	    case 0x8: 
	      if (daemon_enabled)alsa_send_note(false, 1, data[i+2], data[i+3]);
	      break;

	    case 0x9: // Just fwd note info
	      if (daemon_enabled)alsa_send_note(true, 1, data[i+2], data[i+3]);
	      break;

	    default:
	      if (!silentf) if (logfd) fprintf(logfd, "usb rx other %02x %02x %02x %02x\n", data[i],  data[i+1], data[i+2], data[i+3]);
	      break;
	    }
	}
    }
  else 
    {
      if (rc ==  LIBUSB_ERROR_NO_DEVICE || rc == LIBUSB_ERROR_PIPE) 
	{
	  if (noretryf)
	    {
	      syslog (LOG_INFO, "controlsurface-deamon: surface contact lost\n");
	      runmode = S_EXIT;
	    }
	  else runmode = S_CONNECT; // most likely it has been unplugged - will need to reconnect
	}
      else if (rc < 0) // another error:
	{
	  sleep(2);
	  daemon_enabled = 0;
	  runmode = S_REFRESH;
	}
      //error();
    }
}



int mapper_alsa_open()
{
  int rc = snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0);
  if (rc)
    {
      printf("Could not open sequencer, rc=%i\n", rc);
      return -1;
    }

  rc = snd_seq_set_client_name(seq_handle, alsa_name_exported);
  if (rc)
    {
      printf("Could not set client name, rc=%i\n", rc);
      return -1;
    }

 
  alsa_in = snd_seq_create_simple_port(seq_handle, "Midi 1",
				       SND_SEQ_PORT_CAP_WRITE|
				       SND_SEQ_PORT_CAP_SUBS_WRITE|
					SND_SEQ_PORT_CAP_READ| // others can read
					SND_SEQ_PORT_CAP_SUBS_READ,
				       SND_SEQ_PORT_TYPE_APPLICATION);
  if (alsa_in < 0)
    {
      printf("Could not set alsa input port\n");
      return -1;
    }

  alsa_out = alsa_in;

  DEBUG(printf ("Opened alsa virtual midi devices ok\n"));  
  return 0;
}


void handle_mmc_idx(int idx) // MMC has been received from alsa and needs echoing to control surface leds:
{
  static int last_on = -1;
  if (!xport_keys_enabled) return; // Do not update it when transport lock is off
  int t = inverted_mmc_index[idx];
  if (t<0)
    {
      printf("No LED set up to reflect MMC status %i\n", idx);
      return;
    }
  if (last_on != -1)        // radio button-style - need to turn off the last one.
    {
      struct knob_s &kold = Knobs[last_on];
      usbtx.send_cc(15, kold.feedback_ctrl, 0); 
    }
  last_on = t;
  struct knob_s &k = Knobs[t];
  struct knob_s::soft_s &s = k.softs[softctx];  
  //if (s.flags & F_FEEDBACK_TO_SURFACE)
  printf("MMC feedback turn on led no %x\n", k.feedback_ctrl);
  usbtx.send_cc(15, k.feedback_ctrl, 127); // TODO radio button-style - need to turn off the others.     
}


// As a control surface we expect to receive MMC echos but we can also update our leds on commands. 
void handle_rx_mmc(const unsigned char *buffer, int l) // Arg is pre-stripped of F0 leader and f7 tailer.
{
  if (buffer[2] != 0x6) return; // buffer 2 is 0x06 for a commmand TODO responses too.
  int code = buffer[3];
  DEBUG(fprintf(logfd, "RX'd MMC cmd=%x\n", code));
  // = {  0x7F, 0x7f, 0x06, 0x0 }; First 0x7F is MMC, Second 0x7F is all devices.
  if (l == 4) switch(code) 
    {
    case MIDI_MMC_Stop: 
      handle_mmc_idx(MMC_STOP_IDX);
      break;
     
    case MIDI_MMC_Punch_Out:
    case MIDI_MMC_Deferred_Play:
    case MIDI_MMC_Play: 
      handle_mmc_idx(MMC_PLAY_IDX);
      break;

    case MIDI_MMC_Pause:
      handle_mmc_idx(MMC_PAUSE_IDX);
      break;
      
    case MIDI_MMC_Punch_In:
    case MIDI_MMC_Record_Ready: 
      handle_mmc_idx(MMC_RECORD_IDX);
      break;
      
    case MIDI_MMC_Rewind: 
      handle_mmc_idx(MMC_REW_IDX);
      break;
      
    case MIDI_MMC_Fast_Forward:
      handle_mmc_idx(MMC_FFWD_IDX);
      break;
      
    default: printf("Ignored MMC command 0x%x\n", code); 
    }

  //                                 0  1  2  3  4  5  6  7  8  9  A
  // O2R96Serial exclusive 12 bytes: 7F 0  6 44  6  1 60 40  6 2e  0  0  when going to locate point 6:46 (0x06, 0x2e)
  else if (l == 12 && code == 0x44)
    {
      int dest = buffer[10] * (1000/25) // frames : always 25 fps ?
	+ buffer[9] * 1000 // seconds
	+ buffer[8] * 60 * 1000 // minutes
	+ (buffer[7] & 0x1F) * 24 * 60 * 1000; // hours
      DEBUG(fprintf(logfd, "MMC Seek dest %i", dest));
    }

  else if (l == 8 && code == 0x46)
    {
      int d = (buffer[6]&7)<<((buffer[6]>>3)&7); // Floating point input.
      if (buffer[6] & 0x40) d = -d;
      // Shuttle 0: 7F 0 6 46 3 70 9 0 0  counts in the field
      DEBUG(fprintf(logfd, "MMC Shuttle %i", d));
    }
  else if (l == 8 && code == 0x46)
    { 
      //      int d = (buffer[5]&7)<<((buffer[5]>>3)&7);
      //if (buffer[5] & 0x40) d = -d;
      // scrub 30/70: 0 6 46 3 30 2 7f 0  	    
      //printf("Scrub %i\n", d);
    }
  else printf("Other MMC rx'd   len=%i  code=0x%X(6 for cmd)  cmd=0x%02X\n", l, buffer[2], buffer[3]);
}


void handle_alsa_exclusive(unsigned char *exclusive_buffer, int len)
{
  switch (exclusive_buffer[0])
    {
    case MIDI_EXC_MMC_7F:
      if (exclusive_buffer[2] == 0x06) // Transport control command
	{
	  handle_rx_mmc(exclusive_buffer, len);
	}
      break;
    }
}


//
//
void handle_alsa_cc(int ch, int cc, int vale)
{      // need to lookup in inverted_index to see if there is a controller mapped to this
  int t = inverted_cc_index[ch][cc];
  static bool ignored[16][128];
  if (t<0) 
    {
      if (!ignored[ch][cc]) printf("Ignoring messge to control surface on ch=%i cc=%i vale=%i (decimal)\n", ch+1, cc, vale);
      ignored[ch][cc] = true;
      return;
    }
  struct knob_s &k = Knobs[t];
  k.remote_flying_update(vale);
}

void knob_s::flush_to_surface(bool drainf)
{
  if ((knob_flags & KF_ALSO_XPORT) && xport_keys_enabled) 
    return; // Button is used for transport control bank so do not update it when transport lock is on
  
  struct knob_s::soft_s &s = softs[softctx];

  if (0 && s.MMC_index > 0)
    {

    }
  else if (feedback_ctrl >= 0 && (knob_flags & KF_UPDATE))
    {
       usbtx.fmt_cc(15, feedback_ctrl, s.last_notification);
       DEBUG(fprintf(logfd, "Flush led to surface %s led=%i vale=%i\n", name, feedback_ctrl, s.last_notification));
    }
  if (drainf) usbtx.flush();
}


void knob_s::remote_flying_update(int vale) // Flying fader or led status remote update
{
  struct knob_s::soft_s &s = softs[softctx];

  if (knob_flags & KF_UPDATE)
    {
       if (knob_flags & KF_NOVATION_ENCODER)
	 {
            // Novataion encoders have 12 settings (0 to 11) so values from 0..127 must be scaled to 0..11
            int vale1 = vale;
            if (knob_flags & KF_NOVATION_ENCODER) vale1 = (vale * 11)/127;
            s.last_notification  = vale1;
            flush_to_surface();
         }
       else 
	 {
            s.last_notification  = vale;
            flush_to_surface();
         }
    }

  if (s.flags & F_PICKUP_ENABLE)
    {
      s.pickup_target = vale;
      s.pickup_set = true;
      DEBUG(fprintf(logfd, "Marked %s for pickup at %i\n", name, vale));
    }
}




//
// Here we receive flying fader and status LED updates.
//
void alsa_dispatch(const snd_seq_event_t *ev)
{
  static int limitter = 0;
  if (ev->type == SND_SEQ_EVENT_ECHO) 
    {
      //      flash();
      //send_echo(ev->time.tick + 10);
    }
  else if (ev->type ==SND_SEQ_EVENT_SYSEX)
    {
      int len = ev->data.ext.len;
      unsigned char *d = (unsigned char *)ev->data.ext.ptr;
      DEBUG(for (int i =0; i<len; i++) printf("%02X ", d[i]); printf(": exclusive msg received\n"));
	if (len >= 3) handle_alsa_exclusive(d+1, len-2); // Skip leading 0xF0 and final 0xF7
    }
  
  else if((ev->type == SND_SEQ_EVENT_NOTEON)||(ev->type == SND_SEQ_EVENT_NOTEOFF)) 
    {
      const char *type = (ev->type==SND_SEQ_EVENT_NOTEON) ? "on " : "off";
      if (limitter++<100)
	printf("[%d] Ignore note %s: %2x vel(%2x)\n", ev->time.tick, type,
	       ev->data.note.note,
	       ev->data.note.velocity);
    }
  else if(ev->type == SND_SEQ_EVENT_CONTROLLER)
    {
      int channel = ev->data.control.channel;
      // This is all OMNI mode - we ignore the channel number comming in!
      int cc = ev->data.control.param;
      int vale = ev->data.control.value;
      DEBUG(fprintf(logfd, "alsa_rx    ALSA->Surface  ch=%i cc=0x%x vale=%i\n", channel+1, cc, vale));
      handle_alsa_cc(channel, cc, vale);
    }
  else if(limitter++ < 100)
    printf("Unhandled alsa event received type %i\n", ev->type);
}




void *surface_to_alsa(void *arg)
{
   while(runmode != S_EXIT)
     {
      if (runmode == S_CONNECT)
	{
	  int rc = open_novation_usb_dev();
	  if (!rc) runmode = S_REFRESH;
	  else sleep(5);
	}
      else if (runmode == S_RUN) usb_dispatch();
     }
    return 0;
}


void *alsa_to_surface(void *arg)
{
  snd_seq_event_t *ev = NULL;
  snd_seq_event_input(seq_handle, &ev);
  if (daemon_enabled) alsa_dispatch(ev);
}

void make_inverted_index()
{
  for (int i =0; i< 128; i++)
    {
      struct knob_s &k = Knobs[i];  
      struct knob_s::soft_s &s = k.softs[softctx];
      if (s.fwd_ctrl >= 0 && s.fwd_ctrl < 128) inverted_cc_index[s.fwd_chan][s.fwd_ctrl] = i;
      if (s.MMC_index > 0 && s.MMC_index < MMC_INDEXES) inverted_mmc_index[s.MMC_index] = i;
    }
}

int knob_being_created = 0;

// User settings call this once per control surface entity
void define_knob_hard(int knob, const char *name, unsigned short knob_flags, int companion)
{
  assert(knob >= 0 && knob < 128);
  knob_being_created = knob;
  struct knob_s &k = Knobs[knob];
  k.name = name;
  k.knob_flags = knob_flags;
  k.companion_ctrl = companion;
}

// User settings call this once per virtual controller mapped to the currently being created control surface entity
void define_knob_soft(int softctx, unsigned short soft_flags, int channel, int controller, int mmc_index, int feedback_d) //Call this for each of the soft contexts for the current index, or once with a negative arg.
{
  assert(channel >= 1 && channel <= 16);
  assert(controller >= 0 && controller <= 127);
  struct knob_s &k = Knobs[knob_being_created];
  for(int i=0;i<SOFT_CONTEXTS;i++)
    {
      if (softctx >= 0 && softctx != i) continue; // all contexts denoted with -ve setting - -1 same channel for each ctx, -2 sucsessive channels for each ctx. 
      struct knob_s::soft_s &s = k.softs[i];
      //if (softctx >= 0 && softctx < SOFT_CONTEXTS)
      s.flags = soft_flags;
      s.fwd_chan = channel-1 + (softctx == -2 ? i:0); 
      s.fwd_ctrl = controller;
      k.feedback_ctrl = feedback_d ? feedback_d: knob_being_created;
      s.MMC_index = mmc_index; // Should be 0 if F_MMC missing.
    }
}


// Novation-specific code:
// Send the Novation exclusive to turn on automap - there is also a simple parameter change you can send instread.
void say_automap_on()
{
  unsigned char automap_on[] = { 0xF0, 0x00, 0x20, 0x29, 0x03, 0x03, 0x12, 0x00, 0x02, 0x00, 0x01, 0x01, 0xF7 };
  usbtx.send_sysex(automap_on, sizeof automap_on);
}

// Novation-specific code:
// Send the following message to the LCD.
// TODO: forward or transcode channel name exclusives from standard DAW codes to label the surface.
void say_msg(const char *msg)
{
  unsigned char line[MAXL];
  int len = strlen(msg);
  unsigned char lcd[] = { 0xF0, 0x00, 0x20, 0x29, 0x03, 0x03, 0x12, 0x00, 0x02, 0x00, 0x02, 0x02, 0x04, 0x01, 0x09, 0x01, 0x04 };
  assert(len+sizeof lcd < MAXL-2); 
  memcpy(line, lcd, sizeof lcd);
   sprintf((char *)line+sizeof lcd, "%s%c%c", msg, 0x00, 0xF7); 
   usbtx.send_sysex(line, sizeof lcd + len + 2);
}


// Display the current context message on the LCD of the surface.
void lcd_update()
{
  say_msg(banners[softctx]);
}


//A fader that has been remotely 'moved' but which has no motor and so cannot actually move
//needs to enter 'pickup mode'. In pickup mode, when the user moves it, some LEDs flash and no
//events are sent on to the controlled target.  The LEDs indicate which direction the user must
//move it to catch up with the current value and when it is put there it exits pickup mode.


void flashx_t::stop()
{
  countdown = 0;
  DEBUG2(printf(" flasher stop %i\n", flasher));
  if (flasher) usbtx.send_cc(15, pickup_flash_ctrl, 0);
  flasher = 0;
}

void flashx_t::run10Hz()
{
  if (countdown > 0)
    {
      countdown -= 1;
      flasher = countdown > 1 ? !flasher:0;
      DEBUG(fprintf(logfd, " flasher hi=%i status=%i\n", hi, flasher));
      // Novation-specific code here:
      // We have five LEDs down the left side - we can use the lower two for 'too high/move down' indication and the upper
      // two for 'too low/move up' indication.  We can flash the middle one briefly when the pickup point is reached.
      int code = flasher? ((hi) ? 0x04: 0x02):
	0;
      usbtx.send_cc(15, pickup_flash_ctrl, code);
    }
}


int daemon()
{
  syslog (LOG_INFO, "controlsurface-deamon: startup\n");
  logfd = 0; //stderr;//fopen("/tmp/slog", "w");
  //fclose(stdout); fclose(stdin); fclose(stderr);
  //setlinebuf(logfd);
  if (logfd) fprintf(logfd, "Opened\n");
  for (int ch=0;ch<16;ch++) for (int i=0; i<128; i++) inverted_cc_index[ch][i] = -1;
  for (int i=0; i<MMC_INDEXES; i++) inverted_mmc_index[i] = -1;
  
  MMC_codes[MMC_PLAY_IDX] = MIDI_MMC_Play;
  MMC_codes[MMC_PAUSE_IDX] = MIDI_MMC_Pause;
  MMC_codes[MMC_RECORD_IDX] = MIDI_MMC_Record_Ready;
  MMC_codes[MMC_FFWD_IDX] = MIDI_MMC_Fast_Forward;
  MMC_codes[MMC_REW_IDX] = MIDI_MMC_Rewind;
  MMC_codes[MMC_STOP_IDX] = MIDI_MMC_Stop;
  
  int err = libusb_init(&ctx);
  if (err) {
    fprintf(stderr, "unable to initialize libusb: %i\n", err);
    return EXIT_FAILURE;
  }
  load_settings(0);
  int rc = mapper_alsa_open();
  if (rc) return EXIT_FAILURE;
  
  rc = open_novation_usb_dev();
  if (rc) return EXIT_FAILURE;
  
  make_inverted_index();
  
  int npfd = snd_seq_poll_descriptors_count(seq_handle, POLLIN);
  struct pollfd *pfd = (struct pollfd *)alloca(npfd * sizeof(struct pollfd));
  snd_seq_poll_descriptors(seq_handle, pfd, npfd, POLLIN);
  
  pthread_t otherdir;
  pthread_create(&otherdir, 0, surface_to_alsa, 0);
  
  pthread_t flasher_thr;
  pthread_create(&flasher_thr, 0, flash, 0);
  runmode = S_REFRESH;

  syslog (LOG_INFO, "controlsurface-deamon: running\n");
  while (runmode != S_EXIT)
    {
      if (runmode == S_REFRESH) 
 	{
 	  //  sayhello();
 	  say_automap_on(); // Device should respond with 6B 01
 	  flush_to_surface();
 	  runmode = S_RUN;
	  //flashx.trigger(1); // spurious trigger
 	}

      if (poll(pfd, npfd, 500) > 0)  // timeout is in ms.
 	{
 	  alsa_to_surface(0);
 	}  
      //char line[132];
      //snprintf(line, 132, "controlsurface-deamon: tick %i %i %i bg=%i\n", getgid(), spid, runmode, bg);
      //syslog (LOG_INFO, line);
    }
  libusb_release_interface(xdev, subifc);
  syslog (LOG_INFO, "controlsurface-deamon: exiting\n");
  return 0;
}


int main(int argc, const char *argv[])
{
  int fg = 0, bg = 0;


  while (argc >= 2)
    {
      //printf("Consider %s\n", argv[1]);
      if (argc >= 2 && !strcmp(argv[1], "--noretry")) 
	{
	  noretryf = 1;
	  argc --; argv ++; continue;
	}
      else if (argc >= 2 && !strcmp(argv[1], "--foreground"))
	{
	  fg = 1; argc --; argv ++; continue;
	}
      else if (argc >= 2 && !strcmp(argv[1], "--daemon"))
	{
	  bg = 1; argc --; argv++; continue;
	}
      else if (argc >= 2) 
	{
	  fprintf(stderr, "%s: bad arg: '%s'\n", alsa_name_exported, argv[1]);
	  exit(-2);
	}
      else break;
    }

  silentf = bg;

  if (!fg && !bg) 
    {
      fprintf(stderr, "%s: bad arg: need --foreground or --daemon\n", alsa_name_exported);
      exit(-3);
    }

  if (fg) daemon();

  else if (bg)
    {
      if (!fork()) 
	{
	  // various attempts to stop udev from killing the daemon after a timeout - all failed on my machine.
	  //int a = getpid();
	  //setpgid(a, a);
	  //setgid(a); 
	  daemon();
	}
    }
  return 0;
}
// eof
