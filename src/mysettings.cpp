#include "daemon.h"

// ControlSurface-Daemon
//
// ControlSurface-Daemon: A  Mapper Daemon for Novation Control Surfaces and others where management is needed.
//
// A daemon that exports an ALSA midi client that speaks over a private USB connection to a studio control surface
//
// (C) 2014 David J Greaves - University of Cambridge / Mixerton ST
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//

// Suitable settings for a Novation  ZeRO MkII


const char *banners[SOFT_CONTEXTS] =
  { "Context 0: Channels 1  2  3  4    5  6  7  8",
    "Context 1: Channels 9 10  11 12   13 14 15 16",
    "Context 2: Channels 17 18 19 20   21 22 23 24",
    "Context 3: Channels 25 26 27 28   29 30 31 32"
  };

const char *alsa_name_exported = "NovationMapper";

void load_settings(int ignored_)
{
  define_knob_hard(0x1, "LearnKey",  KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); // Diversions via BIT6 come to these six.
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 32, -1, 0/*D*/); 
  define_knob_hard(0x2, "ViewKey",   KF_BOOL|KF_UPDATE/*A*/, 0/*B*/);
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 33, -1, 0/*D*/);   
  define_knob_hard(0x3, "UserKey",   KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 34, -1, 0/*D*/); 
  define_knob_hard(0x4, "FxKey",     KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 35, -1, 0/*D*/); 
  define_knob_hard(0x5, "InstKey",   KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 36, -1, 0/*D*/); 
  define_knob_hard(0x6, "MixerKey",  KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2, 37, -1, 0/*D*/);   


  define_knob_hard(0x08, "LeftRotary1", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,9, -1, 0/*D*/); 
  define_knob_hard(0x09, "LeftRotary2", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,10, -1, 0/*D*/); 
  define_knob_hard(0x0a, "LeftRotary3", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,11, -1, 0/*D*/); 
  define_knob_hard(0x0b, "LeftRotary4", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,12, -1, 0/*D*/); 
  define_knob_hard(0x0c, "LeftRotary5", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,13, -1, 0/*D*/); 
  define_knob_hard(0x0d, "LeftRotary6", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,14, -1, 0/*D*/); 
  define_knob_hard(0x0e, "LeftRotary7", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,15, -1, 0/*D*/); 
  define_knob_hard(0x0f, "LeftRotary8", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,16, -1, 0/*D*/); 



  define_knob_hard(0x10, "RightSlider1", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1 ,1, -1, 0/*D*/); 
  define_knob_hard(0x11, "RightSlider2", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 2, -1, 0/*D*/); 
  define_knob_hard(0x12, "RightSlider3", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 3, -1, 0/*D*/); 
  define_knob_hard(0x13, "RightSlider4", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 4, -1, 0/*D*/); 
  define_knob_hard(0x14, "RightSlider5", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 5, -1, 0/*D*/); 
  define_knob_hard(0x15, "RightSlider6", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 6, -1, 0/*D*/); 
  define_knob_hard(0x16, "RightSlider7", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 7, -1, 0/*D*/); 
  define_knob_hard(0x17, "RightSlider8", 0/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1, 8, -1, 0/*D*/); 


  define_knob_hard(0x18, "LeftUpperButton1", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 1+31, -1, 0/*D*/); 
  define_knob_hard(0x19, "LeftUpperButton2", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 2+31, -1, 0/*D*/); 
  define_knob_hard(0x1a, "LeftUpperButton3", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 3+31, -1, 0/*D*/); 
  define_knob_hard(0x1b, "LeftUpperButton4", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 4+31, -1, 0/*D*/); 
  define_knob_hard(0x1c, "LeftUpperButton5", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 5+31, -1, 0/*D*/); 
  define_knob_hard(0x1d, "LeftUpperButton6", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 6+31, -1, 0/*D*/); 
  define_knob_hard(0x1e, "LeftUpperButton7", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 7+31, -1, 0/*D*/); 
  define_knob_hard(0x1f, "LeftUpperButton8", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 8+31, -1, 0/*D*/); 

  define_knob_hard(0x20, "LeftLowerButton1", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1, 9+31, -1, 0/*D*/); 
  define_knob_hard(0x21, "LeftLowerButton2", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,10+31, -1, 0/*D*/); 
  define_knob_hard(0x22, "LeftLowerButton3", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,11+31, -1, 0/*D*/); 
  define_knob_hard(0x23, "LeftLowerButton4", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,12+31, -1, 0/*D*/); 
  define_knob_hard(0x24, "LeftLowerButton5", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,13+31, -1, 0/*D*/); 
  define_knob_hard(0x25, "LeftLowerButton6", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,14+31, -1, 0/*D*/); 
  define_knob_hard(0x26, "LeftLowerButton7", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,15+31, -1, 0/*D*/); 
  define_knob_hard(0x27, "LeftLowerButton8", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,16+31, -1, 0/*D*/); 


  define_knob_hard(0x28, "RightUpperButton1", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,17+31, -1, 0/*D*/); 
  define_knob_hard(0x29, "RightUpperButton2", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,18+31, -1, 0/*D*/); 
  define_knob_hard(0x2a, "RightUpperButton3", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,19+31, -1, 0/*D*/); 
  define_knob_hard(0x2b, "RightUpperButton4", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,20+31, -1, 0/*D*/); 
  define_knob_hard(0x2c, "RightUpperButton5", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,21+31, -1, 0/*D*/); 
  define_knob_hard(0x2d, "RightUpperButton6", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,22+31, -1, 0/*D*/); 
  define_knob_hard(0x2e, "RightUpperButton7", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,23+31, -1, 0/*D*/); 
  define_knob_hard(0x2f, "RightUpperButton8", KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,24+31, -1, 0/*D*/); 

  define_knob_hard(0x30, "RightLowerButton1", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,25+31, -1, 0/*D*/); 
  define_knob_hard(0x31, "RightLowerButton2", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,26+31, -1, 0/*D*/); 
  define_knob_hard(0x32, "RightLowerButton3", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,27+31, -1, 0/*D*/); 
  define_knob_hard(0x33, "RightLowerButton4", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,28+31, -1, 0/*D*/); 
  define_knob_hard(0x34, "RightLowerButton5", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,29+31, -1, 0/*D*/); 
  define_knob_hard(0x35, "RightLowerButton6", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,30+31, -1, 0/*D*/); 
  define_knob_hard(0x36, "RightLowerButton7", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,31+31, -1, 0/*D*/); 
  define_knob_hard(0x37, "RightLowerButton8", KF_ALSO_XPORT|KF_BOOL|KF_UPDATE/*A*/, 0/*B*/); 
  define_knob_soft(-2/*repeat*/, F_FWD_TO_ALSA/*C*/, 1,32+31, -1, 0/*D*/); 


  define_knob_hard(0x40, "SustainPedal", KF_BOOL/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2,2, -1, 0/*D*/); 
  define_knob_hard(0x41, "ExpressionPedal", KF_BOOL/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 2,3, -1, 0/*D*/); 


  define_knob_hard(0x78, "LeftInfinite1", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x70/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,49, -1, 0/*D*/); 
  define_knob_hard(0x79, "LeftInfinite2", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x71/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,50, -1, 0/*D*/); 
  define_knob_hard(0x7A, "LeftInfinite3", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x72/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,51, -1, 0/*D*/); 
  define_knob_hard(0x7B, "LeftInfinite4", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x73/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,52, -1, 0/*D*/); 
  define_knob_hard(0x7C, "LeftInfinite5", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x74/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,53, -1, 0/*D*/); 
  define_knob_hard(0x7D, "LeftInfinite6", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x75/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,54, -1, 0/*D*/); 
  define_knob_hard(0x7E, "LeftInfinite7", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x76/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,55, -1, 0/*D*/); 
  define_knob_hard(0x7F, "LeftInfinite8", KF_NOVATION_ENCODER|KF_UPDATE/*A*/, 0x77/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,56, -1, 0/*D*/); 

  // Transport lock button codes share controller numbers with automap
  // buttons and are distinguished with bit 6 of the vale field.
  define_knob_hard(0x48, "RewindKey", KF_BIT6/*A*/, 1/*B*/); 
  define_knob_soft(-1, F_MMC/*C*/, 4,1, MMC_REW_IDX, 0x30/*D*/); 
  define_knob_hard(0x49, "FFWDKey", KF_BIT6/*A*/, 2/*B*/); 
  define_knob_soft(-1, F_MMC/*C*/, 4,2, MMC_FFWD_IDX, 0x31/*D*/); 
  define_knob_hard(0x4a, "StopKey", KF_BIT6/*A*/, 3/*B*/); 
  define_knob_soft(-1, F_MMC/*C*/, 4,3, MMC_STOP_IDX, 0x32/*D*/); 
  define_knob_hard(0x4b, "PlayKey", KF_BIT6/*A*/, 4/*B*/); 
  define_knob_soft(-1, F_MMC/*C*/, 4,4, MMC_PLAY_IDX, 0x33/*D*/); 
  define_knob_hard(0x4c, "LoopKey", KF_BIT6|KF_BOOL/*A*/, 5/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,122, -1, 0x34/*D*/); 
  define_knob_hard(0x4d, "RecordKey", KF_BIT6/*A*/, 6/*B*/); 
  define_knob_soft(-1, F_MMC/*C*/, 4,6, MMC_RECORD_IDX, 0x35/*D*/); 
  // These BIT6 settings just redirect vale>-64 to the alternate (otherwise unused) controller numbers.

  define_knob_hard(0x65, "SpeedDiaButton",KF_BOOL/*A*/, 0/*B*/); 
  define_knob_soft(-1, F_FWD_TO_ALSA/*C*/, 1,119, -1, 0/*D*/); 

  define_knob_hard(0x66, "SpeedDial", KF_CTX_DIAL|KF_NOVATION_ENCODER/*A*/, 0/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA/*C*/, 1,121, -1, 0/*D*/); 

  define_knob_hard(0x42, "Xfader", 0/*A*/, 0/*B*/); 
  define_knob_soft(0, F_FWD_TO_ALSA|F_PICKUP_ENABLE/*C*/, 1,120, -1, 0/*D*/); 


}
