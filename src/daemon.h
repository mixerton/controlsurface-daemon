#ifndef DAEMON_H
#define DAEMON_H

// ControlSurface-Daemon
//
// ControlSurface-Daemon: A  Mapper Daemon for Novation Control Surfaces and others where management is needed.
//
// A daemon that exports an ALSA midi client that speaks over a private USB connection to a studio control surface
//
// (C) 2014 David J Greaves - University of Cambridge / Mixerton ST
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//

#include "mysurface.h"

extern const char *alsa_name_exported;

#define MIDI_MMC_Stop 		0x01
#define MIDI_MMC_Play            2
#define MIDI_MMC_Deferred_Play	 3
#define MIDI_MMC_Fast_Forward 	 4
#define MIDI_MMC_Rewind		 5
#define MIDI_MMC_Punch_In        6 
#define MIDI_MMC_Record_Strobe  0x06 
#define MIDI_MMC_Punch_Out       7
#define MIDI_MMC_Exit_Punch_Out 0x07
#define MIDI_MMC_Record_Ready 	0x08 // Or record_pause
#define MIDI_MMC_Pause 		0x09
#define MIDI_MMC_Eject		0x0A
#define MIDI_MMC_Chase 		0x0b
#define MIDI_MMC_Reset 		0x0d
#define MIDI_MMC_Write 		0x40
#define MIDI_MMC_Locate_GoTo 	0x44
#define MIDI_Shuttle 		0x47

#define MIN_PERC_NOTE 35
#define MAX_PERC_NOTE 85

#define MF_CONTROL_MOD (1)
#define MF_CONTROL_VOLUME  (7)
#define MF_CONTROL_PAN (10)
#define MF_CONTROL_EXP (11)
#define MF_CONTROL_BANK_MSB (0)
#define MF_CONTROL_BANK_LSB (32)
#define MF_CONTROL_SUSTAIN_PEDAL (64)
#define MF_CONTROL_PORT_PEDAL (65)
#define MF_CONTROL_SOS_PEDAL (66)
#define MF_CONTROL_SOFT_PEDAL (67)
#define MF_CONTROL_LEGATO_PEDAL (68)
#define MF_CONTROL_HOLD_PEDAL (69)

#define MF_CONTROL_ALL_NOTES_OFF (123)
#define MF_CONTROL_MONO (126)
#define MF_CONTROL_POLY (127)
#define MIDI_OFF 0x80
#define MIDI_ON 0x90
#define MIDI_PRESSURE 0xA0
#define MIDI_PARAMETER 0xB0
#define MIDI_PROGCHANGE 0xC0
#define MIDI_CHANPRESSURE 0xD0
#define MIDI_PITCH_BEND_MAX 0x3FFF
#define MIDI_PITCH_BEND_MIN 0x0000
#define MIDI_PITCH_BEND_ZERO 0x2000 /* 0x2000 = 8192 */

#define MIDI_EXC_MMC_7F 0x7F
// Standard MIDI Files use a pitch wheel range of +/-2 semitones = 200 cents.
// MIDI pitch bend wheel resolution (according to the spec) is +8192/-8191.
#define MIDI_PITCHBEND 0xE0
#define MIDI_CLOCK 0xF8

#define MIDI_START 0xFA
#define MIDI_CONT 0xFB
#define MIDI_STOP  0xFC

#define MIDI_ACTIVE_SENSE 0xFE
#define MIDI_SYS_RESET 0xFF

#define MF_TEXT_TEXT 1
#define MF_TEXT_COPYRIGHT 2
#define MF_TEXT_TRKNAME 3
#define MF_TEXT_INSTRNAME 4
#define MF_TEXT_LYRIC 5
#define MF_TEXT_MARKER 6
#define MF_TEXT_CUE 7

#define F_FWD_TO_ALSA 1
#define F_PICKUP_ENABLE 2
#define F_MMC 4

#define KF_NOVATION_ENCODER 1  // Novataion encoders have 12 settings (0 to 11) so values from 0..127 must be scaled to 0..11
#define KF_BOOL 2

#define KF_ALSO_XPORT 4 // Button is used for transport control bank so do not updated it when transport lock is on
#define KF_CTX_DIAL 8  // Change soft context
#define KF_CTX_UP 16
#define KF_CTX_DOWN 32 
#define KF_BIT6 64
#define KF_UPDATE 128
#define MAXL 1024

extern void define_knob_hard(int knob, const char *name, unsigned short knob_flags, int companion);
extern void define_knob_soft(int softctx, unsigned short soft_flags, int channel, int controller, int spare0, int spare);

#define SOFT_CONTEXTS 4


extern void load_settings(int which);

#define MMC_UNUSED_IDX 0
#define MMC_PAUSE_IDX 1      
#define MMC_RECORD_IDX 2      
#define MMC_FFWD_IDX 3    
#define MMC_REW_IDX 4
#define MMC_STOP_IDX 5
#define MMC_PLAY_IDX 6      
#define MMC_INDEXES 7

#endif
