#ifndef MYSURFACE_H
#define MYSURFACE_H

// All of the following information can be easily seen from a USB sniffer program:
// Custom settings for a particular control surface.


// ControlSurface-Daemon
//
// ControlSurface-Daemon: A  Mapper Daemon for Novation Control Surfaces and others where management is needed.
//
// A daemon that exports an ALSA midi client that speaks over a private USB connection to a studio control surface
//
// (C) 2014 David J Greaves - University of Cambridge / Mixerton ST
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//


// Novation ZeRO II values:
const int manu = 0x1235;        // USB manufacturer numbers (Novation).
const int devid = 0x000c;       // USB device encoding number (ZeRO II).
const int subifc = 2;           // Which USB subinterface to use 
const int endpoint = 6;         // 
const int wire_number = subifc; // Which virtual MIDI device inside the control surface endpoint

const int record_led = 0x61; //
const int pickup_flash_ctrl = 0x60; //  used for pickup indication - bit mask
const int novation_automap_button_cc = 0x6b;
const int novation_xport_button_cc   = 79;

// Codes for finger touch: 108, 109, 110, 111 - touch sensitive notification for controller groups.

// Pokes for the controller ring modes are cc=0x78 to 0x7F.  Send vale=0x00 for normal, 0x10 for anticlockwise and vale=0x40 for PAN.

#endif
